//
//  CoreApi.h
//  CoreApi
//
//  Created by Thibault Wittemberg on 17-01-08.
//  Copyright © 2017 Starfleet. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoreApi.
FOUNDATION_EXPORT double CoreApiVersionNumber;

//! Project version string for CoreApi.
FOUNDATION_EXPORT const unsigned char CoreApiVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreApi/PublicHeader.h>


