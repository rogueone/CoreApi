//
//  City.swift
//  CoreApi
//
//  Created by Thibault Wittemberg on 17-01-08.
//  Copyright © 2017 Starfleet. All rights reserved.
//

import Foundation

public struct City {
    var name: String
    
    public init (name: String) {
        self.name = name
    }
}
